package main

import (
	"fmt"
	"kustomize-poc/config"
	"kustomize-poc/handlers"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	config, err := config.LoadConfig(".")
	if err != nil {
		log.Fatal("Cannot read config", err)
	}

	fmt.Println("Port is\t\t", config.Server.Port)
	fmt.Println("EXAMPLE_VAR is\t", config.EXAMPLE_VAR)

	r := gin.Default()
	r.GET("/", handlers.ShowToIndex)

	r.Run()

}
