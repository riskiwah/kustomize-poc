package handlers

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/exec"

	"github.com/gin-gonic/gin"
)

func commitHash() (string, error) {
	cmd := exec.Command("git", "rev-parse", "--short", "HEAD")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	if err := cmd.Run(); err != nil {
		return "", fmt.Errorf("%w; %s", err, stderr.String())
	}
	return stdout.String(), nil
}

func ShowToIndex(c *gin.Context) {
	hostname, err := os.Hostname()
	commit, err := commitHash()
	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, gin.H{
		"commit":   commit,
		"hostname": hostname,
	})
}
